package umg.edu.progra.pilas;

/**
 *
 * @author Walter Cordova
 */
public class NodoPila {

    int elemento;
    NodoPila siguiente;

    @Override
    public String toString() {
        return "NodoPila{" + "elemento=" + elemento + ", siguiente=" + siguiente + '}';
    }

    NodoPila(int x) {
        elemento = x;
        siguiente = null;
    }
}
