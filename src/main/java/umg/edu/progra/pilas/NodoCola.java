package umg.edu.progra.pilas;

/**
 *
 * @author Walter Cordova
 */
public class NodoCola {

    int elemento;
    NodoCola siguiente;

    @Override
    public String toString() {
        return "NodoCola{" + "elemento=" + elemento + ", siguiente=" + siguiente + '}';
    }

    public NodoCola(int elemento) {
        this.elemento = elemento;
        this.siguiente = null;
    }

}

class ColaLista {

    protected NodoCola frente;
    protected NodoCola fin;

    @Override
    public String toString() {
        return "ColaLista{" + "frente=" + frente + ", fin=" + fin + '}';
    }

    public ColaLista() {
        frente = fin = null;
    }

    public void insertar(int elemento) {
        NodoCola a;
        a = new NodoCola(elemento);

        if (colaVacia()) {
            frente = a;
        } else {
            fin.siguiente = a;
        }
        fin = a;
    }

    public int quitar() {
        int aux;
        if (!colaVacia()) {
            aux = frente.elemento;
            frente = frente.siguiente;
        } else {
            return -1;
        }

        return aux;
    }

    public void buscarCola() {
        while (frente != null) {
            frente = frente.siguiente;
        }

    }

    public boolean colaVacia() {
        return (frente == null);
//        if (frente == null) {
//            return true;
//        } else {
//            return false;
//        }
    }

}
