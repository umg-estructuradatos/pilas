package umg.edu.progra.pilas;

/**
 *
 * @author Walter Cordova
 */
public class Principal {

    public static void main(String[] args) {

        //pila();
        cola();

    }

    /**
     * LIFO
     */
    public static void pila() {

        PilaLista pila = new PilaLista();

        pila.insertar(1);
        pila.insertar(2);
        pila.insertar(3);
        pila.insertar(4);
        pila.insertar(5);
        pila.insertar(6);
        pila.insertar(7);

        System.out.println("" + pila);

        System.out.println("" + pila.quitar());
        System.out.println("" + pila.quitar());
        System.out.println("" + pila.quitar());
        System.out.println("" + pila.quitar());
        System.out.println("" + pila.quitar());

        System.out.println("" + pila);
//        
//        System.out.println("" + pila);
//        pila.limpiarPila();
//        System.out.println("" + pila);

    }

    /**
     * FIFO
     */
    public static void cola() {

        ColaLista cola = new ColaLista();

        cola.insertar(1);
        cola.insertar(2);
        cola.insertar(3);
        cola.insertar(4);
        cola.insertar(5);
        cola.insertar(6);
        cola.insertar(7);

        System.out.println("" + cola);

        System.out.println("" + cola.quitar());//1
        System.out.println("" + cola);
        System.out.println("" + cola.quitar());//2
        System.out.println("" + cola.quitar());//3
        System.out.println("" + cola.quitar());//4
        System.out.println("" + cola.quitar());//5
        cola.insertar(12);
        cola.insertar(25);
        cola.insertar(9);
        System.out.println("" + cola.quitar());//4
        System.out.println("" + cola.quitar());//4
        System.out.println("" + cola.quitar());//4
        System.out.println("" + cola.quitar());//4
        System.out.println("" + cola.quitar());//4
        System.out.println("" + cola.quitar());//4
        
        System.out.println("" + cola);

    }

}
